<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>SISTEMA FAWARIS</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/fawarisLogo2.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

<style type="text/css">
.contenedor:hover .imagen {-webkit-transform:scale(1.3);transform:scale(1.3);}
.contenedor {overflow:hidden;}
</style>
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="header">
    <div class="container">

      <div id="logo" class="pull-left">
        <h1><a href="index.html" class="scrollto">FAWARIS</a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="index.html"><img src="assets/img/logo.png" alt=""></a> -->
      </div>

      <nav id="nav-menu-container">
        <ul class="nav-menu">
          <li class="menu-active"><a href="home.php"><img src="img/home.png" width="20" height="20">Home</a></li>
          <li><a href="Pedidos.php"><img src="img/pedidos.png" width="20" height="20">Pedidos</a></li>
           <li class="menu-has-children"><a href=""><img src="img/ventas.png" width="20" height="20">Ventas</a>
            <ul>
              <li class="menu-has-children" ><a href="#"><img src="img/punto-de-venta.png" width="20" height="20">Punto de venta</a></li>
              <li class="menu-has-children" ><a href="GraficaPedidosVentas.php"><img src="img/grafica.png" width="20" height="20">Gráfico de ventas</a></li>
            </ul>
          </li>
          <li><a href="#pricing"><img src="img/almacen.png" width="20" height="20">Almacen</a></li>
          <li><a href="#team"><img src="img/mensajes.png" width="20" height="20">Mensajes</a></li>
          
          <li class="menu-has-children"><a href=""><img src="img/sistemaabc.png" width="20" height="20">Catálogos</a>
            <ul>
              <li class="menu-has-children" ><a href="#"><img src="img/agregar.png" width="20" height="20">Altas</a>
                <ul>
                  <li><a href="insertarProducto.php"><img src="img/producto.png" width="20" height="20">Productos</a></li>
                  <li><a href="#"><img src="img/cliente.png" width="20" height="20">Clientes</a></li>
                  <li><a href="#"><img src="img/empleado.png" width="20" height="20">Empleados</a></li>
                  <li><a href="#"><img src="img/proveedor.png" width="20" height="20">Proveedores</a></li>
                  <li><a href="#"><img src="img/persona.png" width="20" height="20">Personas de Contacto</a></li>
                </ul>
              </li>
              <li class="menu-has-children"><a href="#"><img src="img/eliminar.png" width="20" height="20">Bajas</a>
                <ul>
                  <li><a href="#"><img src="img/producto.png" width="20" height="20">Productos</a></li>
                  <li><a href="#"><img src="img/cliente.png" width="20" height="20">Clientes</a></li>
                  <li><a href="#"><img src="img/empleado.png" width="20" height="20">Empleados</a></li>
                  <li><a href="#"><img src="img/proveedor.png" width="20" height="20">Proveedores</a></li>
                  <li><a href="#"><img src="img/persona.png" width="20" height="20">Personas de Contacto</a></li>
                </ul>
              </li>
              <li class="menu-has-children"><a href="#"><img src="img/listar.png" width="20" height="20">Consultas</a>
                <ul>
                  <li><a href="ListarProductos.php"><img src="img/producto.png" width="20" height="20">Productos</a></li>
                  <li><a href="ListarClientes.php"><img src="img/cliente.png" width="20" height="20">Clientes</a></li>
                  <li><a href="ListarEmpleados.php"><img src="img/empleado.png" width="20" height="20">Empleados</a></li>
                  <li><a href="ListarProveedores.php"><img src="img/proveedor.png" width="20" height="20">Proveedores</a></li>
                  <li><a href="ListarPersonasContacto.php"><img src="img/persona.png" width="20" height="20">Personas de Contacto</a></li>
                </ul>
              </li>
              <li class="menu-has-children"><a href="#"><img src="img/actualizar.png" width="20" height="20">Actualizaciones</a>
                 <ul>
                  <li><a href="#"><img src="img/producto.png" width="20" height="20">Productos</a></li>
                  <li><a href="#"><img src="img/cliente.png" width="20" height="20">Clientes</a></li>
                  <li><a href="#"><img src="img/empleado.png" width="20" height="20">Empleados</a></li>
                  <li><a href="#"><img src="img/proveedor.png" width="20" height="20">Proveedores</a></li>
                  <li><a href="#"><img src="img/persona.png" width="20" height="20">Personas de Contacto</a></li>
                </ul>
              </li>
            
            </ul>
          </li>
          <li><a href="#contact"><img src="img/contactar.png" width="20" height="20">Contact Us</a></li>
          <li><a href="logout.php"><img src="img/salir.png" width="20" height="20"></a></li>
          
        </ul>
      </nav><!-- #nav-menu-container -->
    </div>
  </header><!-- End Header -->


  <main id="main">

        <!-- ======= About Section ======= -->
    <section id="about" class="section-bg">
      <div class="container-fluid">

        <div class="row">
          <div class="col-lg-4 about-img wow fadeInLeft contenedor">
            <br>
            <br> 
            <br>
            <br>
            <img src="images/productosI.jpg" width="500" height="400" class="imagen" alt="Imagen de Alexas_Fotos en Pixabay">
          </div>

        <div class="col-lg-8 content wow fadeInRight">
           <div class="form">
           <br>
           <br> 
             <h1>Registrar un nuevo producto</h1>
             <p>Para dar el alta un nuevo producto, es necesario requisitar los siguientes campos.</p> 
              <form action="insertarP.php" method="post" role="form">
                <div class="form-row">
                  <div class="form-group col-lg-2">
                    <label>ID</label>
                    <input type="text" name="id" id="id" class="form-control"  placeholder="Identificador" data-rule="minlen:4" data-msg="El id debe tener como mínimo 4 carácteres">
                  </div>
                  <div class="form-group col-lg-4">
                    <label>Nombre del Producto:</label>
                    <input type="text" name="name" class="form-control"  placeholder="Nombre. Ejemlo Galletas" data-rule="minlen:4" data-msg="El nombre debe tener como mínimo 4 carácteres">
                    <div class="validate"></div>
                  </div>
                  <div class="form-group col-lg-4">
                    <label>Precio de Venta</label>
                    <input type="number" name="precioVenta" id="precioVenta" class="form-control"  placeholder="Precio de Venta" data-rule="minlen:4" data-msg="El nombre debe tener como mínimo 4 carácteres">
                    <div class="validate"></div>
                  </div>
                  <div class="form-group col-md-4">
                    <label for="inputState">Sección</label>
                      <select id="saccion" name="seccion" class="form-control" onchange="carg(this);">
                       <option value="Seleccione.." selected>Seleccione..</option>
                       <option value="Bebidas">Bebidas</option>
                       <option value="Botana">Botana</option>
                       <option value="Galletas">Galletas</option>
                       <option value="Granos">Granos</option>
                       <option value="Higiene">Higiene</option>
                       <option value="Lacteos">Lacteos</option>
                       <option value="Limpieza">Limpieza</option>
                       <option value="Origen Animal">Origen Animal</option>
                       <option value="Otro">Otro</option>
                      </select>
                  </div>
                  <div class="form-group col-lg-4">
                    <label>Otra sección</label>
                    <input type="text" id="otro" name="otro" class="form-control" disabled="">
                  </div>
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" name="subject" id="subject" placeholder="Asunto" data-rule="minlen:4" data-msg="El asusto no puede estar vacío">
                  <div class="validate"></div>
                </div>
                <div class="form-group">
                  <textarea class="form-control" name="descripcion" rows="5" data-rule="required" data-msg="El mesaje no puede estar vacío" placeholder="Descripción del producto"></textarea>
                  <div class="validate"></div>
                </div>
                <div class="text-center"><button type="submit" title="Send Message"  class="btn btn-success">
                  <span class="glyphicon glyphicon-check"></span> Guardar</button></div>
              </form>
            </div>
          </div>
        </div>

      </div>
    </section><!-- End About Section -->


  </main><!-- End #main -->

<!-- ======= Footer ======= -->
  <footer id="footer">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 text-lg-left text-center">
          <div class="copyright">
            &copy; Copyright <strong>FAWARIS</strong>. Todos los derechos reservados
          </div>
          <div class="credits">
            <!--
            All the links in the footer should remain intact.
            You can delete the links only if you purchased the pro version.
            Licensing information: https://bootstrapmade.com/license/
            Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Avilon
         
            Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a> -->
          </div>
        </div>
        <div class="col-lg-6">
          <nav class="footer-links text-lg-right text-center pt-2 pt-lg-0">
            <a href="#intro" class="scrollto">Home</a>
            <a href="#about" class="scrollto">Acerca</a>
            <a href="#">Pol&iacute;ticas de Privacidad</a>
            
          </nav>
        </div>
      </div>
    </div>
  </footer><!-- End  Footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/wow/wow.min.js"></script>
  <script src="assets/vendor/venobox/venobox.min.js"></script>
  <script src="assets/vendor/superfish/superfish.min.js"></script>
  <script src="assets/vendor/hoverIntent/hoverIntent.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>
  <script type="text/javascript">
    var input = document.getElementById('otro');

function carg(elemento) {
  d = elemento.value;
  
  if(d == "Otro"){
    otro.disabled = false;
  }else{
    otro.disabled = true;
  }
}
  </script>

</body>

</html>