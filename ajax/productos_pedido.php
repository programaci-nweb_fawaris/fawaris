<?php

	 ini_set('error_reporting', E_ERROR | E_WARNING | E_STRICT);
	 error_reporting(0);
	/* Connect To Database*/
	require_once ("../config/db.php");//Contiene las variables de configuracion para conectar a la base de datos
	require_once ("../config/conexion.php");//Contiene funcion que conecta a la base de datos
	
	$action = (isset($_REQUEST['action'])&& $_REQUEST['action'] !=NULL)?$_REQUEST['action']:'';
	if($action == 'ajax'){
		// escaping, additionally removing everything that could be (html/javascript-) code
         $q = mysqli_real_escape_string($con,(strip_tags($_REQUEST['q'], ENT_QUOTES)));
		 $aColumns = array('idProducto', 'NombreProducto');//Columnas de busqueda
		 $proveedor=$_GET['proveedor'];
	         printf($proveedor);
		 //$sTable = "producto";
		 $sTable = "producto_proveedor inner join producto as p on p.idProducto=id_Producto inner join proveedor on idProveedor=id_Proveedor where id_Proveedor=".$proveedor;
		 $sWhere = "";
		if ( $_GET['q'] != "" )
		{
			$sWhere = "and (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				$sWhere .= $aColumns[$i]." LIKE '%".$q."%' OR ";
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		include 'pagination.php'; //include pagination file
		//pagination variables
		$page = (isset($_REQUEST['page']) && !empty($_REQUEST['page']))?$_REQUEST['page']:1;
		$per_page = 7; //how much records you want to show
		$adjacents  = 4; //gap between pages after number of adjacents
		$offset = ($page - 1) * $per_page;
		//Count the total number of row in your table*/
		$query1="SELECT count(*) AS numrows FROM $sTable  $sWhere";
		//echo "".$query1;
		$count_query   = mysqli_query($con, $query1);
		$row= mysqli_fetch_array($count_query);
		$numrows = $row['numrows'];
		$total_pages = ceil($numrows/$per_page);
		$reload = './index.php';
		//main query to fetch the data
		$sql="SELECT P.*,Precio_compra FROM  $sTable $sWhere LIMIT $offset,$per_page";
		//echo "2. ".$sql;
		$query = mysqli_query($con, $sql);
		//loop through fetched data
		if ($numrows>0){
			
			?>
			<div class="table-responsive">
			  <table class="table">
				<tr  class="warning">
					<th>Código</th>
					<th>Producto</th>
					<th>Sección</th>
					<th>Descripción</th>
					<th><span class="pull-right">Stock</span></th>
					<th><span class="pull-right">Cant.</span></th>
					<th><span class="pull-right">Precio Unitario</span></th>
					<th style="width: 36px;"></th>
				</tr>
				<?php
				while ($row=mysqli_fetch_array($query)){
					$id_producto=$row['idProducto'];
					$nombre_producto=$row['NombreProducto'];
					$seccion_producto=$row['Seccion'];
					$stock=$row['Stock'];
					$descripcion=$row['Descripcion'];


					/*$codigo_producto=$row['codigo_producto'];
					$id_marca_producto=$row['id_marca_producto'];
					$codigo_producto=$row["codigo_producto"];
					$sql_marca=mysqli_query($con, "select nombre_marca from marcas where id_marca='$id_marca_producto'");
					$rw_marca=mysqli_fetch_array($sql_marca);
					$nombre_marca=$rw_marca['nombre_marca'];*/
					$precio_venta=$row["Precio_compra"];
					$precio_venta=number_format($precio_venta,2);
					?>
					<tr>
						<td><?php echo $id_producto; ?></td>
						<td><?php echo $nombre_producto; ?></td>
						<td ><?php echo $seccion_producto; ?></td>
						<td ><?php echo $descripcion; ?></td>
						<td class='col-xs-1'>
						<div class="pull-right">
						<input type="text" class="form-control" style="text-align:right" value="<?php echo $stock; ?>">
						</div></td>


						<td class='col-xs-1'>
						<div class="pull-right">
						<input type="text" class="form-control" style="text-align:right" id="cantidad_<?php echo $id_producto; ?>"  value="1" >
						</div></td>
						<td class='col-xs-2'><div class="pull-right">
						<input type="text" readonly="readonly" style="text-align:right" id="precio_venta_<?php echo $id_producto; ?>"  value="<?php echo $precio_venta;?>" >
						</div></td>
						<td ><span class="pull-right"><a href="#" onclick="agregar('<?php echo $id_producto ?>')"><i class="glyphicon glyphicon-plus"></i></a></span></td>
					</tr>
					<?php
				}
				?>
				<tr>
					<td colspan=5><span class="pull-right"><?php
					 echo paginate($reload, $page, $total_pages, $adjacents);
					?></span></td>
				</tr>
			  </table>
			</div>
			<?php
		}
	}
?>