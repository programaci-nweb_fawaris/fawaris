<?PHP
   session_start ();
   if (isset($_SESSION["usuario_valido"]))
   {
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>SISTEMA FAWARIS</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/fawarisLogo2.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700|Open+Sans:300,300i,400,400i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">


</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="header-transparent">
    <div class="container">

      <div id="logo" class="pull-left">
        <h1><a href="index.html" class="scrollto">FAWARIS</a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="index.html"><img src="assets/img/logo.png" alt=""></a> -->
      </div>

      <nav id="nav-menu-container">
        <ul class="nav-menu">
          <li class="menu-active"><a href="home.php"><img src="img/home.png" width="20" height="20">Home</a></li>
          <li><a href="Pedidos.php"><img src="img/pedidos.png" width="20" height="20">Pedidos</a></li>

          <li class="menu-has-children"><a href=""><img src="img/ventas.png" width="20" height="20">Ventas</a>
            <ul>
              <li class="menu-has-children" ><a href="#"><img src="img/punto-de-venta.png" width="20" height="20">Punto de venta</a></li>
              <li class="menu-has-children" ><a href="GraficaPedidosVentas.php"><img src="img/grafica.png" width="20" height="20">Gráfico de ventas</a></li>
            </ul>
          </li>

          <li><a href="#pricing"><img src="img/almacen.png" width="20" height="20">Almacen</a></li>
          <li><a href="#team"><img src="img/mensajes.png" width="20" height="20">Mensajes</a></li>
          
          <li class="menu-has-children"><a href=""><img src="img/sistemaabc.png" width="20" height="20">Catálogos</a>
            <ul>
              <li class="menu-has-children" ><a href="#"><img src="img/agregar.png" width="20" height="20">Altas</a>
                <ul>
                  <li><a href="insertarProducto.php"><img src="img/producto.png" width="20" height="20">Productos</a></li>
                  <li><a href="#"><img src="img/cliente.png" width="20" height="20">Clientes</a></li>
                  <li><a href="#"><img src="img/empleado.png" width="20" height="20">Empleados</a></li>
                  <li><a href="#"><img src="img/proveedor.png" width="20" height="20">Proveedores</a></li>
                  <li><a href="#"><img src="img/persona.png" width="20" height="20">Personas de Contacto</a></li>
                </ul>
              </li>
              <li class="menu-has-children"><a href="#"><img src="img/eliminar.png" width="20" height="20">Bajas</a>
                <ul>
                  <li><a href="#"><img src="img/producto.png" width="20" height="20">Productos</a></li>
                  <li><a href="#"><img src="img/cliente.png" width="20" height="20">Clientes</a></li>
                  <li><a href="#"><img src="img/empleado.png" width="20" height="20">Empleados</a></li>
                  <li><a href="#"><img src="img/proveedor.png" width="20" height="20">Proveedores</a></li>
                  <li><a href="#"><img src="img/persona.png" width="20" height="20">Personas de Contacto</a></li>
                </ul>
              </li>
              <li class="menu-has-children"><a href="#"><img src="img/listar.png" width="20" height="20">Consultas</a>
                <ul>
                  <li><a href="ListarProductos.php"><img src="img/producto.png" width="20" height="20">Productos</a></li>
                  <li><a href="ListarClientes.php"><img src="img/cliente.png" width="20" height="20">Clientes</a></li>
                  <li><a href="ListarEmpleados.php"><img src="img/empleado.png" width="20" height="20">Empleados</a></li>
                  <li><a href="ListarProveedores.php"><img src="img/proveedor.png" width="20" height="20">Proveedores</a></li>
                  <li><a href="ListarPersonasContacto.php"><img src="img/persona.png" width="20" height="20">Personas de Contacto</a></li>
                </ul>
              </li>
              <li class="menu-has-children"><a href="#"><img src="img/actualizar.png" width="20" height="20">Actualizaciones</a>
                 <ul>
                  <li><a href="#"><img src="img/producto.png" width="20" height="20">Productos</a></li>
                  <li><a href="#"><img src="img/cliente.png" width="20" height="20">Clientes</a></li>
                  <li><a href="#"><img src="img/empleado.png" width="20" height="20">Empleados</a></li>
                  <li><a href="#"><img src="img/proveedor.png" width="20" height="20">Proveedores</a></li>
                  <li><a href="#"><img src="img/persona.png" width="20" height="20">Personas de Contacto</a></li>
                </ul>
              </li>
            
            </ul>
          </li>
          <li><a href="#contact"><img src="img/contactar.png" width="20" height="20">Contact Us</a></li>
          <li><a href="logout.php"><img src="img/salir.png" width="20" height="20"></a></li>
          
        </ul>
      </nav><!-- #nav-menu-container -->
    </div>
  </header><!-- End Header -->

  <!-- ======= Intro Section ======= -->
  <section id="intro">

    <div class="intro-text">
      <h2>Bienvenido a FAWARIS</h2>
      <p>SISTEMA PARA LA ADMINISTRACIÓN DE NEGOCIOS DEDICADOS A LA VENTA DE ARTÍCULOS EN GENERAL</p>
      <a href="#about" class="btn-get-started scrollto">Iniciar..</a>
    </div>

    <div class="product-screens">

      <div class="product-screen-1 wow fadeInUp" data-wow-delay="0.4s" data-wow-duration="0.6s">
        <img src="assets/img/fawarisLogo.png" width="400" height="400" alt="">
      </div>

      <div class="product-screen-2 wow fadeInUp" data-wow-delay="0.2s" data-wow-duration="0.6s">
        <img src="assets/img/image1.jpg" width="250" height="250" alt="">
      </div>

      <div class="product-screen-3 wow fadeInUp" data-wow-duration="0.6s">
        <img src="assets/img/image3.jpg"  width="250" height="250" alt="">
      </div>

    </div>

  </section><!-- End Intro Section -->

  <main id="main">

    <!-- ======= About Section ======= -->
    <section id="about" class="section-bg">
      <div class="container-fluid">
        <div class="section-header">
          <h3 class="section-title"><b>Bienvenid@: </b></h3><h2 class="section-title"> <?php  echo $_SESSION["nombre"]." ". $_SESSION["apellidoP"] ." ". $_SESSION["apellidoM"]; ?> </h2>
          <span class="section-divider"></span>
          <p class="section-description">
            La tarea de un buen manager, gerente o director es hacer que los grupos de personas trabajen motivados, ya que su satisfacción personal al trabajar está directamente relacionada con la productividad.
            <strong> «El talento gana partidos, pero el trabajo en equipo y la inteligencia gana campeonatos», Michael Jordan.</strong>
          </p>
        </div>

        <div class="row">
          <div class="col-lg-6 about-img wow fadeInLeft">
            <img src="assets/img/innovacion.jpg" alt="Foto Usuario">
          </div>

          <div class="col-lg-6 content wow fadeInRight">
            <?php/* include "Pedidos.php";*/?>
            <h2>Datos Generales</h2>
            <?php
             include "./SistemaABC/Conexion.php";
             include "./SistemaABC/Modelo/Empleado.php";
             include "./SistemaABC/Controlador/ControladorEmpleado.php";
             $c = new ControladorEmpleado();
             $empleado=$c->seleccionarID($_SESSION["idEmpleado"]);
            ?>

            <ul>
              <li><i class="ion-android-checkmark-circle"></i><strong>ID Empleado: </strong><?php echo " ".$empleado->getIdEmpleado() ?></li>
              <li><i class="ion-android-checkmark-circle"></i><strong>Nombre Completo: </strong><?php echo " ".$empleado->getNombre()." ".$empleado->getApellidoPaterno()." ".$empleado->getApellidoMaterno(); ?></li>
              <li><i class="ion-android-checkmark-circle"></i><strong>Dirección: </strong><?php echo " ".$empleado->getCalle()." ". $empleado->getNumero(); ?></li>
              <li><i class="ion-android-checkmark-circle"></i><strong>Puesto de Trabajo: </strong><?php echo " ".$empleado->getCategoria(); ?></li>
              <li><i class="ion-android-checkmark-circle"></i><strong>Teléfono: </strong><?php echo " ".$empleado->getTel_movil(); ?></li>

              <?php echo "<li><i class='ion-android-checkmark-circle'></i><strong>Fecha de Nacimiento: </strong><input type='date' min='".$empleado->getFecha_Nacimiento()."' max='".$empleado->getFecha_Nacimiento()."'  value='".$empleado->getFecha_Nacimiento()."'></li>"; ?>

            </ul>
            <?php
               $c->close_Connection();
            ?>

            <p>
              En caso de querer hacer una modificación a la información que se muestra, cuenta con la opción de <strong> Actualizar</strong>, está opción le permitirá hacer cualquier modificación de su información personal.
            </p>

            <div class="text-center"><button type="submit" title="Actualizar info"  class="btn btn-warning" >Actualizar Datos</button></div>
          </div>
        </div>

      </div>
    </section><!-- End About Section -->

   
    <!-- ======= Contact Section ======= -->
    <section id="contact">
      <div class="container">
        <div class="row wow fadeInUp">

          <div class="col-lg-4 col-md-4">
            <div class="contact-about">
              <h3>FAWARIS</h3>
              <p>El sistema FAWARIS, es un sistema transaccional, que permite automatizar la forma en que se se registran las ventas, llevar a cabo el control del almac&eacute;n generar pedidos a los proveedores y comunicar las ofertas a los clientes.</p>
              <div class="social-links">
                <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
                <a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
                <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-4">
            <div class="info">
              <div>
                <i class="ion-ios-location-outline"></i>
                <p>Instituto Tecnot&oacute;gico de Oaxaca<br>Oaxaca, Riveras del Atoyac</p>
              </div>

              <div>
                <i class="ion-ios-email-outline"></i>
                <p>fawaris.ito@gmail.com</p>
              </div>

              <div>
                <i class="ion-ios-telephone-outline"></i>
                <p>951 394 7589</p>
              </div>

            </div>
          </div>

          <div class="col-lg-5 col-md-8">
            <div class="form">
              <form action="forms/contact.php" method="post" role="form" class="php-email-form">
                <div class="form-row">
                  <div class="form-group col-lg-6">
                    <input type="text" name="name" class="form-control" id="name" placeholder="Nombre" data-rule="minlen:4" data-msg="El nombre debe tener como mínimo 4 carácteres">
                    <div class="validate"></div>
                  </div>
                  <div class="form-group col-lg-6">
                    <input type="email" class="form-control" name="email" id="email" placeholder="Email" data-rule="email" data-msg="Ingrese un email válido">
                    <div class="validate"></div>
                  </div>
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" name="subject" id="subject" placeholder="Asunto" data-rule="minlen:4" data-msg="El asusto no puede estar vacío">
                  <div class="validate"></div>
                </div>
                <div class="form-group">
                  <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="El mesaje no puede estar vacío" placeholder="Message"></textarea>
                  <div class="validate"></div>
                </div>
                <div class="mb-3">
                  <div class="loading">Loading</div>
                  <div class="error-message"></div>
                  <div class="sent-message">Your message has been sent. Thank you!</div>
                </div>
                <div class="text-center"><button type="submit" title="Send Message">Enviar Mensaje</button></div>
              </form>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Contact Section -->

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 text-lg-left text-center">
          <div class="copyright">
            &copy; Copyright <strong>FAWARIS</strong>. Todos los derechos reservados
          </div>
          <div class="credits">
            <!--
            All the links in the footer should remain intact.
            You can delete the links only if you purchased the pro version.
            Licensing information: https://bootstrapmade.com/license/
            Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Avilon
         
            Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a> -->
          </div>
        </div>
        <div class="col-lg-6">
          <nav class="footer-links text-lg-right text-center pt-2 pt-lg-0">
            <a href="#intro" class="scrollto">Home</a>
            <a href="#about" class="scrollto">Acerca</a>
            <a href="#">Pol&iacute;ticas de Privacidad</a>
            
          </nav>
        </div>
      </div>
    </div>
  </footer><!-- End  Footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/wow/wow.min.js"></script>
  <script src="assets/vendor/venobox/venobox.min.js"></script>
  <script src="assets/vendor/superfish/superfish.min.js"></script>
  <script src="assets/vendor/hoverIntent/hoverIntent.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>
  <?PHP

   }
   else
   {
      header('location: SinAcceso.php'); 
   }
   
?>

</body>

</html>
