<?PHP
//Maneo de Mensajes de Error
ini_set('error_reporting', E_ERROR | E_WARNING| E_STRICT);

// Iniciar sesión
   session_start();

// Si se ha enviado el formulario
   $usuario = $_REQUEST['usuario'];
   $clave = $_REQUEST['clave'];
   if (isset($usuario) && isset($clave))
   {

  // Conectar con el servidor de base de datos
      $conexion =  new mysqli("localhost:3306","root","INFORMATICA2\$bg","supermercado");
      /* comprobar la conexión */
      if (mysqli_connect_errno()) {
         printf("Falló la conexión: %s\n", mysqli_connect_error());
          exit();
      }
      $salt = substr ($usuario, 0, 2);
     // $clave_crypt = crypt ($clave, $salt);
      $instruccion = "select * from empleado where login = '$usuario'" .
         " and password = '$clave'";
      $consulta = $conexion->query ($instruccion);;

      $nfilas = $consulta->num_rows;
     
      /*print("Usuario: ". $usuario);
      print("Contraseña". $clave);
      print("La contraseña: ". $clave_crypt);*/

   // Los datos introducidos son correctos
      if ($nfilas > 0)
      {
         $usuario_valido = $usuario;
         // Con register_globals On
         // session_register ("usuario_valido");
         // Con register_globals Off
         $_SESSION["usuario_valido"] = $usuario_valido;
         if ($fila = $consulta->fetch_assoc()) {
	      $_SESSION["idEmpleado"] = $fila["idEmpleado"];
              $_SESSION["nombre"] = $fila["Nombre"];
              $_SESSION["apellidoP"] = $fila["ApellidoPaterno"];
              $_SESSION["apellidoM"] = $fila["ApellidoMaterno"];
      }
      }
       $conexion->close ();
   }
?>

<!DOCTYPE HTML> 
<HTML LANG="es">
<HEAD>
<TITLE>Sistema FAWARIS</TITLE>
   <meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->   
   <link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
   <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
   <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
   <link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
   <link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->   
   <link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
   <link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
   <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->   
   <link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
   <link rel="stylesheet" type="text/css" href="css/util.css">
   <link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
</HEAD>

<BODY>
   <div class="limiter">
      <div class="container-login100" style="background-image: url('images/bg-01.jpg');">

         <div class="wrap-login100 p-l-55 p-r-55 p-t-65 p-b-54">


<?PHP
// Sesión iniciada
   if (isset($_SESSION["usuario_valido"]))
   {
   			header('location: home.php'); 
   }

// Intento de entrada fallido
   else if (isset ($usuario))
   {
      print ("<BR><BR>\n");
      print ("<P ALIGN='CENTER'>Acceso no autorizado</P>\n");
      print ("<P ALIGN='CENTER'>[ <A HREF='index.php'>Conectar</A> ]</P>\n");
   }

// Sesión no iniciada
   else
   {   

      print("<BR><BR>\n");
      print("<P>Esta zona tiene el acceso restringido." .
         " Para entrar debe identificarse. <br></P>\n");
      print("<span class='login100-form-title p-b-49'>
                  Login
               </span>");
   
      print("<FORM class='login100-form validate-form' NAME='login' ACTION='index.php' METHOD='POST'>");

       print("<div class='wrap-input100 validate-input m-b-23' data-validate='El USUARIO es Requerido'>");
      print("<span class='label-input100'>Usuario</span>");
      print("<input class='input100' type='text' name='usuario' placeholder='Ingresa tu usuario'>");
      print("<span class='focus-input100' data-symbol='&#xf206;'></span>");
      print("</div>");

      print("<div class='wrap-input100 validate-input' data-validate='La CONTRASEÑA es requerida'>");
      print("<span class='label-input100'>Password</span>");
      print("<input class='input100' type='password' name='clave' placeholder='Ingresa tu contraseña'>");
      print("<span class='focus-input100' data-symbol='&#xf190;'></span>");
      print("</div>");
      print("<br>");




    /*  print("<P><LABEL CLASS='etiqueta-entrada'>Usuario:</LABEL>\n");
      print("   <INPUT TYPE='TEXT' NAME='usuario' SIZE='15'></P>\n");
      print("<P><LABEL CLASS='etiqueta-entrada'>Clave:</LABEL>\n");
      print("   <INPUT TYPE='PASSWORD' NAME='clave' SIZE='15'></P>\n");*/

      print("<div class='container-login100-form-btn'>");
      print("<div class='wrap-login100-form-btn'>");
      print("<div class='login100-form-bgbtn'></div>");

      print("<button class='login100-form-btn'>
                        Login
                     </button>");
      print("</div>");
      print("</div>");
     

      print("</FORM>");
      print("<br>");

      print("<P>NOTA: si no dispone de identificación o tiene problemas " .
         "para entrar<BR>póngase en contacto con el " .
         "<A HREF='MAILTO:webmaster@localhost'>administrador</A> del sitio.</P>\n");

   }
?>
</div>
</div>
</div>


<!--===============================================================================================-->
   <script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
   <script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
   <script src="vendor/bootstrap/js/popper.js"></script>
   <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
   <script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
   <script src="vendor/daterangepicker/moment.min.js"></script>
   <script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
   <script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
   <script src="js/main.js"></script>


</BODY>
</HTML>
