<?PHP
   session_start ();

   if (isset($_SESSION["usuario_valido"]))
   {
   ini_set('error_reporting', E_ERROR | E_WARNING | E_STRICT);
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>SISTEMA FAWARIS</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/fawarisLogo2.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700|Open+Sans:300,300i,400,400i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">
  <style type="text/css">
    #encabezado tr, #encabezado th{
      position: sticky;
      top: 0;
      z-index: 10;
    }
    .table-responsive{
      height: 600px;
      overflow: scroll;
    }
    
  </style>


</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header">
    <div class="container">

      <div id="logo" class="pull-left">
        <h1><a href="index.html" class="scrollto">FAWARIS</a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="index.html"><img src="assets/img/logo.png" alt=""></a> -->
      </div>

      <nav id="nav-menu-container">
        <ul class="nav-menu">
          <li class="menu-active"><a href="home.php"><img src="img/home.png" width="20" height="20">Home</a></li>
          <li><a href="Pedidos.php"><img src="img/pedidos.png" width="20" height="20">Pedidos</a></li>
           <li class="menu-has-children"><a href=""><img src="img/ventas.png" width="20" height="20">Ventas</a>
            <ul>
              <li class="menu-has-children" ><a href="#"><img src="img/punto-de-venta.png" width="20" height="20">Punto de venta</a></li>
              <li class="menu-has-children" ><a href="GraficaPedidosVentas.php"><img src="img/grafica.png" width="20" height="20">Gráfico de ventas</a></li>
            </ul>
          </li>
          <li><a href="#pricing"><img src="img/almacen.png" width="20" height="20">Almacen</a></li>
          <li><a href="#team"><img src="img/mensajes.png" width="20" height="20">Mensajes</a></li>
          
          <li class="menu-has-children"><a href=""><img src="img/sistemaabc.png" width="20" height="20">Catálogos</a>
            <ul>
              <li class="menu-has-children" ><a href="#"><img src="img/agregar.png" width="20" height="20">Altas</a>
                <ul>
                  <li><a href="insertarProducto.php"><img src="img/producto.png" width="20" height="20">Productos</a></li>
                  <li><a href="#"><img src="img/cliente.png" width="20" height="20">Clientes</a></li>
                  <li><a href="#"><img src="img/empleado.png" width="20" height="20">Empleados</a></li>
                  <li><a href="#"><img src="img/proveedor.png" width="20" height="20">Proveedores</a></li>
                  <li><a href="#"><img src="img/persona.png" width="20" height="20">Personas de Contacto</a></li>
                </ul>
              </li>
              <li class="menu-has-children"><a href="#"><img src="img/eliminar.png" width="20" height="20">Bajas</a>
                <ul>
                  <li><a href="#"><img src="img/producto.png" width="20" height="20">Productos</a></li>
                  <li><a href="#"><img src="img/cliente.png" width="20" height="20">Clientes</a></li>
                  <li><a href="#"><img src="img/empleado.png" width="20" height="20">Empleados</a></li>
                  <li><a href="#"><img src="img/proveedor.png" width="20" height="20">Proveedores</a></li>
                  <li><a href="#"><img src="img/persona.png" width="20" height="20">Personas de Contacto</a></li>
                </ul>
              </li>
              <li class="menu-has-children"><a href="#"><img src="img/listar.png" width="20" height="20">Consultas</a>
                <ul>
                  <li><a href="ListarProductos.php"><img src="img/producto.png" width="20" height="20">Productos</a></li>
                  <li><a href="ListarClientes.php"><img src="img/cliente.png" width="20" height="20">Clientes</a></li>
                  <li><a href="ListarEmpleados.php"><img src="img/empleado.png" width="20" height="20">Empleados</a></li>
                  <li><a href="ListarProveedores.php"><img src="img/proveedor.png" width="20" height="20">Proveedores</a></li>
                  <li><a href="ListarPersonasContacto.php"><img src="img/persona.png" width="20" height="20">Personas de Contacto</a></li>
                </ul>
              </li>
              <li class="menu-has-children"><a href="#"><img src="img/actualizar.png" width="20" height="20">Actualizaciones</a>
                 <ul>
                  <li><a href="#"><img src="img/producto.png" width="20" height="20">Productos</a></li>
                  <li><a href="#"><img src="img/cliente.png" width="20" height="20">Clientes</a></li>
                  <li><a href="#"><img src="img/empleado.png" width="20" height="20">Empleados</a></li>
                  <li><a href="#"><img src="img/proveedor.png" width="20" height="20">Proveedores</a></li>
                  <li><a href="#"><img src="img/persona.png" width="20" height="20">Personas de Contacto</a></li>
                </ul>
              </li>
            
            </ul>
          </li>
          <li><a href="#contact"><img src="img/contactar.png" width="20" height="20">Contact Us</a></li>
          <li><a href="logout.php"><img src="img/salir.png" width="20" height="20"></a></li>
          
        </ul>
      </nav><!-- #nav-menu-container -->
    </div>
  </header><!-- End Header -->


  <main id="main">

    <!-- ======= About Section ======= -->
    <section id="about" class="section-bg">
      <div class="container-fluid">
        <div class="section-header">
          <br>
          <br>
          <h3 class="section-title"><b>Lista de Empleados </b></h3>
          <span class="section-divider"></span>
          <p class="section-description">
            Listado de todos los Empleados que se encuentran actualmente registrados en el Sistema, para realizar una búsqueda es necesario introducir el nombre, o el apellido paterno o el apellido materno o id del empleado.
          </p>
          


  <form class="form-horizontal" role="form" id="datos_pedido" action="ListarEmpleados.php" method="GET">
        <div class="row">
          <div class="col-md-2">
          </div>
          <div class="col-md-1">
          <label for="proveedor" class="control-label"><b>Buscar Empleados</b></label>
           
          </div>
          
          <div class="col-md-4">
            <input type="search" class="form-control input-sm" id="barraBusqueda" name="busqueda" placeholder="Nombre del cliente, o apellido (Paterno o Materno)">
          </div>
          
          <div class="col-md-2">

            <button type="submit"  class="btn btn-info">
             <span class="glyphicon glyphicon-plus"></span> Buscar
            </button>
          </div>
          
          <div class="col-md-3">
            <button type="submit" class="btn btn-success">
              <span class="glyphicon glyphicon-print"></span> Mostrar Todos
            </button>

          </div>
              
        </div>
            
      </form>
        <br>

        <div class="row">
          <?php /*
          <div class="col-lg-6 about-img wow fadeInLeft">
            <img src="assets/img/innovacion.jpg" alt="Foto Usuario">
          </div>*/ ?>

          <div class="col-lg-12 content wow fadeInRight">
            <?php 
             $val=$_GET['busqueda'];
             include "./SistemaABC/Conexion.php";
             include "./SistemaABC/Modelo/Empleado.php";
             include "./SistemaABC/Controlador/ControladorEmpleado.php";
             $c = new ControladorEmpleado();
             $empleados=$c->selectAll($val);

            print("<div class='table-responsive'>");

            printf("<table class='table-hover table-sm table-bordered'");
	    printf("<thead id='encabezado'>");
             printf("<tr>"); 
               printf("<th scope='col' style='background:black; color:white;'>#</td>");
               printf("<th scope='col' style='background:black; color:white;'>idEmpleado</td>");
               printf("<th scope='col' style='background:black; color:white;'>Nombre</td>");
               printf("<th scope='col' style='background:black; color:white;'>Apellido Paterno</td>");
               printf("<th scope='col' style='background:black; color:white;'>Apellido Materno</td>");
               printf("<th scope='col' style='background:black; color:white;'>Estado</td>");
               printf("<th scope='col' style='background:black; color:white;'>Municipio</td>");
               printf("<th scope='col' style='background:black; color:white;'>Colonia</td>");
               printf("<th scope='col' style='background:black; color:white;'>Calle</td>");
               printf("<th scope='col' style='background:black; color:white;'>Núm. Domicilio</td>");
               printf("<th scope='col' style='background:black; color:white;'>Tel. Móvil</td>");
               printf("<th scope='col' style='background:black; color:white;'>Fecha Nacimiento</td>");  
             printf("</tr>");
	     printf("</thead>");
             printf("<tbody>");
             $c->close_Connection();
               $x=1;
             foreach ($empleados as $c) {
               printf("<tr>");
                printf("<td>".$x."</td>");
                printf("<td>".$c->getIdEmpleado()."</td>");
                printf("<td>".$c->getNombre()."</td>");
                printf("<td>".$c->getApellidoPaterno()."</td>");
                printf("<td>".$c->getApellidoMaterno()."</td>");
                printf("<td>".$c->getEstado()."</td>");
                printf("<td>".$c->getMunicipio()."</td>");
                printf("<td>".$c->getColonia()."</td>");
                printf("<td>".$c->getCalle()."</td>");
                printf("<td>".$c->getNumero()."</td>");
                printf("<td>".$c->getTel_movil()."</td>");
                printf("<td> <input type='date'  min='".$c->getFecha_Nacimiento()."' max='".$c->getFecha_Nacimiento()."'value='".$c->getFecha_Nacimiento()."'</td>");
               // printf("<td>".$c->getEmail()."</td>");
                printf("</tr>");
                $x++;
            }



            printf("</tbody>");
            printf("</table>");
            printf("</div>");


            ?>
            
          </div>
        </div>

      </div>
    </section><!-- End About Section -->

   
    <!-- ======= Contact Section ======= -->
    <section id="contact">
      <div class="container">
        <div class="row wow fadeInUp">

          <div class="col-lg-4 col-md-4">
            <div class="contact-about">
              <h3>FAWARIS</h3>
              <p>El sistema FAWARIS, es un sistema transaccional, que permite automatizar la forma en que se se registran las ventas, llevar a cabo el control del almac&eacute;n generar pedidos a los proveedores y comunicar las ofertas a los clientes.</p>
              <div class="social-links">
                <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
                <a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
                <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-4">
            <div class="info">
              <div>
                <i class="ion-ios-location-outline"></i>
                <p>Instituto Tecnot&oacute;gico de Oaxaca<br>Oaxaca, Riveras del Atoyac</p>
              </div>

              <div>
                <i class="ion-ios-email-outline"></i>
                <p>info@example.com</p>
              </div>

              <div>
                <i class="ion-ios-telephone-outline"></i>
                <p>+1 5589 55488 55s</p>
              </div>

            </div>
          </div>

          <div class="col-lg-5 col-md-8">
            <div class="form">
              <form action="forms/contact.php" method="post" role="form" class="php-email-form">
                <div class="form-row">
                  <div class="form-group col-lg-6">
                    <input type="text" name="name" class="form-control" id="name" placeholder="Nombre" data-rule="minlen:4" data-msg="Please enter at least 4 chars">
                    <div class="validate"></div>
                  </div>
                  <div class="form-group col-lg-6">
                    <input type="email" class="form-control" name="email" id="email" placeholder="Email" data-rule="email" data-msg="Please enter a valid email">
                    <div class="validate"></div>
                  </div>
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" name="subject" id="subject" placeholder="Asunto" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject">
                  <div class="validate"></div>
                </div>
                <div class="form-group">
                  <textarea class="form-control" name="message" id="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
                  <div class="validate"></div>
                </div>
                <div class="mb-3">
                  <div class="loading">Loading</div>
                  <div class="error-message"></div>
                  <div class="sent-message">Your message has been sent. Thank you!</div>
                </div>
                <div class="text-center"><button type="submit" title="Send Message">Enviar Mensaje</button></div>
              </form>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Contact Section -->

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 text-lg-left text-center">
          <div class="copyright">
            &copy; Copyright <strong>FAWARIS</strong>. Todos los derechos reservados
          </div>
          <div class="credits">
            <!--
            All the links in the footer should remain intact.
            You can delete the links only if you purchased the pro version.
            Licensing information: https://bootstrapmade.com/license/
            Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Avilon
         
            Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a> -->
          </div>
        </div>
        <div class="col-lg-6">
          <nav class="footer-links text-lg-right text-center pt-2 pt-lg-0">
            <a href="#intro" class="scrollto">Home</a>
            <a href="#about" class="scrollto">Acerca</a>
            <a href="#">Pol&iacute;ticas de Privacidad</a>
            
          </nav>
        </div>
      </div>
    </div>
  </footer><!-- End  Footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/wow/wow.min.js"></script>
  <script src="assets/vendor/venobox/venobox.min.js"></script>
  <script src="assets/vendor/superfish/superfish.min.js"></script>
  <script src="assets/vendor/hoverIntent/hoverIntent.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js">
  </script>
  <?PHP

   }
   else
   {
      header('location: SinAcceso.php'); 
   }
   
?>

</body>

</html>
