<?PHP
   session_start ();
   if (isset($_SESSION["usuario_valido"]))
   {
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Sistema Web de Pedidos</title>
   <!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/css/select2.min.css" rel="stylesheet" />
	<link rel=icon href='img/fawarisLogo2.png' sizes="32x32" type="image/png">
	  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700|Open+Sans:300,300i,400,400i,700,700i" rel="stylesheet">
 <link href="assets/css/style.css" rel="stylesheet">


  </head>
  <body>
   <!-- ======= Header ======= -->
  <header id="header">
    <div class="container">

      <div id="logo" class="pull-left">
        <h1><a href="index.html" class="scrollto"><h2><span class="glyphicon glyphicon-edit"></span> Nuevo Pedido</h2></a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="index.html"><img src="assets/img/logo.png" alt=""></a> -->
      </div>

      <nav id="nav-menu-container">
        <ul class="nav-menu">
          <li class="menu-active"><a href="home.php"><img src="img/home.png" width="20" height="20">Home</a></li>
          <li><a href="Pedidos.php"><img src="img/pedidos.png" width="20" height="20">Pedidos</a></li>
           <li class="menu-has-children"><a href=""><img src="img/ventas.png" width="20" height="20">Ventas</a>
            <ul>
              <li class="menu-has-children" ><a href="#"><img src="img/punto-de-venta.png" width="20" height="20">Punto de venta</a></li>
              <li class="menu-has-children" ><a href="GraficaPedidosVentas.php"><img src="img/grafica.png" width="20" height="20">Gráfico de ventas</a></li>
            </ul>
          </li>
          <li><a href="#pricing"><img src="img/almacen.png" width="20" height="20">Almacen</a></li>
          <li><a href="#team"><img src="img/mensajes.png" width="20" height="20">Mensajes</a></li>
          
          <li class="menu-has-children"><a href=""><img src="img/sistemaabc.png" width="20" height="20">Catálogos</a>
            <ul>
              <li class="menu-has-children" ><a href="#"><img src="img/agregar.png" width="20" height="20">Altas</a>
                <ul>
                  <li><a href="insertarProductos.php"><img src="img/producto.png" width="20" height="20">Productos</a></li>
                  <li><a href="#"><img src="img/cliente.png" width="20" height="20">Clientes</a></li>
                  <li><a href="#"><img src="img/empleado.png" width="20" height="20">Empleados</a></li>
                  <li><a href="#"><img src="img/proveedor.png" width="20" height="20">Proveedores</a></li>
                  <li><a href="#"><img src="img/persona.png" width="20" height="20">Personas Cont.</a></li>
                </ul>
              </li>
              <li class="menu-has-children"><a href="#"><img src="img/eliminar.png" width="20" height="20">Bajas</a>
                <ul>
                  <li><a href="#"><img src="img/producto.png" width="20" height="20">Productos</a></li>
                  <li><a href="#"><img src="img/cliente.png" width="20" height="20">Clientes</a></li>
                  <li><a href="#"><img src="img/empleado.png" width="20" height="20">Empleados</a></li>
                  <li><a href="#"><img src="img/proveedor.png" width="20" height="20">Proveedores</a></li>
                  <li><a href="#"><img src="img/persona.png" width="20" height="20">Personas Cont.</a></li>
                </ul>
              </li>
              <li class="menu-has-children"><a href="#"><img src="img/listar.png" width="20" height="20">Consultas</a>
                <ul>
                  <li><a href="ListarProductos.php"><img src="img/producto.png" width="20" height="20">Productos</a></li>
                  <li><a href="ListarClientes.php"><img src="img/cliente.png" width="20" height="20">Clientes</a></li>
                  <li><a href="ListarEmpleados.php"><img src="img/empleado.png" width="20" height="20">Empleados</a></li>
                  <li><a href="ListarProveedores.php"><img src="img/proveedor.png" width="20" height="20">Proveedores</a></li>
                  <li><a href="ListarPersonasContacto.php"><img src="img/persona.png" width="20" height="20">Personas Cont.</a></li>
                </ul>
              </li>
              <li class="menu-has-children"><a href="#"><img src="img/actualizar.png" width="20" height="20">Actualizaciones</a>
                 <ul>
                  <li><a href="#"><img src="img/producto.png" width="20" height="20">Productos</a></li>
                  <li><a href="#"><img src="img/cliente.png" width="20" height="20">Clientes</a></li>
                  <li><a href="#"><img src="img/empleado.png" width="20" height="20">Empleados</a></li>
                  <li><a href="#"><img src="img/proveedor.png" width="20" height="20">Proveedores</a></li>
                  <li><a href="#"><img src="img/persona.png" width="20" height="20">Personas Cont.</a></li>
                </ul>
              </li>
            
            </ul>
          </li>
          <li><a href="home.php #contact"><img src="img/contactar.png" width="20" height="20">Contact Us</a></li>
          <li><a href="logout.php"><img src="img/salir.png" width="20" height="20"></a></li>
          
        </ul>
      </nav><!-- #nav-menu-container -->
    </div>
  </header><!-- End Header -->


    <div class="container">
		  <div class="row-fluid">
		  
			<div class="col-md-12">
		    <br>
		    <br>
		    <br>
			<h2><span class="glyphicon glyphicon-edit"></span> Nuevo Pedido</h2>
			<hr>
			<form class="form-horizontal" role="form" id="datos_pedido">
				<div class="row">
				  
				  <div class="col-md-3">
				  <label for="proveedor" class="control-label">Selecciona el proveedor</label>
					 <select class="proveedor form-control" name="proveedor" id="proveedor" required>
					</select>
				  </div>
				  
					<div class="col-md-3">
						<label for="transporte" class="control-label">Transporte</label>
						<input type="text" class="form-control input-sm" id="transporte" value="Terrestre" required>
					</div>
					
					<div class="col-md-2">
						<label for="condiciones" class="control-label">Condiciones de pago</label>
						<input type="text" class="form-control input-sm" id="condiciones" value="Contado" required>
					</div>
					
					<div class="col-md-4">
						<label for="comentarios" class="control-label">Comentarios</label>
						<input type="text" class="form-control input-sm" id="comentarios" placeholder="Comentarios o instruciones especiales">
					</div>
							
				</div>
						
				
				<hr>
				<div class="col-md-12">
					<div class="pull-right">
						<button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal" onclick="load(1)">
						 <span class="glyphicon glyphicon-plus"></span> Agregar productos
						</button>
						<button type="submit" class="btn btn-success">
						  <span class="glyphicon glyphicon-print"></span> Imprimir
						</button>
						
					</div>	
				</div>
			</form>

				<button class="btn btn-danger" role="link" onclick="window.location='home.php'">
						  <span class="glyphicon glyphicon-arrow-left"></span> Salir
				</button>
			<br><br>
		<div id="resultados" class='col-md-12'></div><!-- Carga los datos ajax -->
	
			<!-- Modal -->
			<div class="modal fade bs-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			  <div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
				  <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">Buscar productos</h4>
				  </div>
				  <div class="modal-body">
					<form class="form-horizontal">
					  <div class="form-group">
						<div class="col-sm-6">
						  <input type="text" class="form-control" id="q" placeholder="Buscar productos" onkeyup="load(1)">
						</div>
						<button type="button" class="btn btn-default" onclick="load(1)"><span class='glyphicon glyphicon-search'></span> Buscar</button>
					  </div>
					</form>
					<div id="loader" style="position: absolute;	text-align: center;	top: 55px;	width: 100%;display:none;"></div><!-- Carga gif animado -->
					<div class="outer_div" ></div><!-- Datos ajax Final -->
				  </div>
				  <div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
					
				  </div>
				</div>
			  </div>
			</div>
			
			</div>	
		 </div>
	</div>

   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
	<script type="text/javascript" src="js/VentanaCentrada.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.min.js"></script>
	<script>
		$(document).ready(function(){
			load(1);
		});

		function load(page){
			
			var q= $("#q").val();
			var proveedor = $("#proveedor").val();
			var parametros={"action":"ajax","page":page,"q":q,"proveedor":proveedor};
			$("#loader").fadeIn('slow');
			$.ajax({
				url:'./ajax/productos_pedido.php',
				data: parametros,
				 beforeSend: function(objeto){
				 $('#loader').html('<img src="./img/ajax-loader.gif"> Cargando...');
			  },
				success:function(data){
					$(".outer_div").html(data).fadeIn('slow');
					$('#loader').html('');
					
				}
			})
		}
	</script>
	<script>
	function agregar (id)
		{
			var proveedor = $("#proveedor").val();
			var precio_venta=$('#precio_venta_'+id).val();
			var cantidad=$('#cantidad_'+id).val();
			//Inicia validacion
			if (isNaN(cantidad))
			{
			alert('Esto no es un numero');
			document.getElementById('cantidad_'+id).focus();
			return false;
			}
			if (isNaN(precio_venta))
			{
			alert('Esto no es un numero');
			document.getElementById('precio_venta_'+id).focus();
			return false;
			}
			//Fin validacion
		var parametros={"id":id,"precio_venta":precio_venta,"cantidad":cantidad,"proveedor":proveedor};	
		$.ajax({
        type: "POST",
        url: "./ajax/agregar_pedido.php",
        data: parametros,
		 beforeSend: function(objeto){
			$("#resultados").html("Mensaje: Cargando...");
		  },
        success: function(datos){
		$("#resultados").html(datos);
		}
			});
		}
		
			function eliminar (id)
		{
			
			$.ajax({
        type: "GET",
        url: "./ajax/agregar_pedido.php",
        data: "id="+id,
		 beforeSend: function(objeto){
			$("#resultados").html("Mensaje: Cargando...");
		  },
        success: function(datos){
		$("#resultados").html(datos);
		}
			});

		}
		
		$("#datos_pedido").submit(function(){
		  var proveedor = $("#proveedor").val();
		  var transporte = $("#transporte").val();
		  var condiciones = $("#condiciones").val();
		  var comentarios = $("#comentarios").val();
		  if (proveedor>0)
		 {
			VentanaCentrada('./pdf/documentos/pedido_pdf.php?proveedor='+proveedor+'&transporte='+transporte+'&condiciones='+condiciones+'&comentarios='+comentarios,'Pedido','','1024','768','true');	
		 } else {
			 alert("Selecciona el proveedor");
			 return false;
		 }
		 
	 	});
	</script>
<script type="text/javascript">
$(document).ready(function() {
    $( ".proveedor" ).select2({        
    ajax: {
        url: "ajax/load_proveedores.php",
        dataType: 'json',
        delay: 250,
        data: function (params) {
            return {
                q: params.term // search term
            };
        },
        processResults: function (data) {
            return {
                results: data
            };
        },
        cache: true
    },
    minimumInputLength: 1

});
});

</script>	


	

<!-- minimumInputLength: 2 Indica el numero minimo de caracteres para cargar los proveedores-->
<?PHP

   }
   else
   {
      header('location: SinAcceso.php'); 
   }
   
?>

  </body>
</html>